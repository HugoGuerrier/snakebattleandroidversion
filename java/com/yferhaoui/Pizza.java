package com.yferhaoui;

import android.graphics.PointF;

public class Pizza extends ObjectPartie {

    public Pizza(final PointF fieldDimensions, final Snake snake, final Float radius) {
        super(radius);
        this.newRandomLocation(fieldDimensions, snake);
    }

    public void newRandomLocation(final PointF fieldDimensions, final Snake snake) {
        final PointF p = new PointF();

        boolean valid = false;
        while (valid == false) {
            valid = true;

            p.x = RandomNumber.nextInteger((int)(fieldDimensions.x) - 2) + 1;
            p.y = RandomNumber.nextInteger((int)(fieldDimensions.y) - 2) + 1;

            for (final Case aCase : snake.getaCases()) {
                if (new Case(p.x, p.y, this.radius).inCase(aCase)) {
                    valid = false;
                    break;
                } valid = true;
            }

        } this.location = p;
    }
}