package com.yferhaoui;

import android.graphics.PointF;

public class ObjectPartie {

    protected PointF location;
    protected final Float radius;

    public ObjectPartie(final Float radius) {
        location = new PointF();
        this.radius = radius;
    }

    public Float getRadius() {

        return radius;
    }

    public PointF getLocation() {

        return location;
    }

    public boolean inCase(final ObjectPartie theCase) {

        float xMax = (theCase.getLocation().x)*this.radius*2 + this.radius*2;
        float yMax = (theCase.getLocation().y)*this.radius*2 + this.radius*2;
        float xMilieu = (this.location.x)*this.radius*2 + this.radius;
        float yMilieu = (this.location.y)*this.radius*2 + this.radius;
        if ( xMilieu < xMax && xMilieu > theCase.getLocation().x*this.radius*2 && yMilieu < yMax && yMilieu > theCase.getLocation().y*this.radius*2 ) {
            return true;
        } return false;

    }

}