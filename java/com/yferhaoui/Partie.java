package com.yferhaoui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import java.util.ArrayDeque;

public class Partie extends Activity {

    private TimerManager.StateAsk stateAsk = TimerManager.StateAsk.PLAY;

    private SurfaceView surface;

    private Paint paint;
    private ArrayDeque<Direction> directionsQueue;

    private PointF dimensions;
    private Float cellsDiameter, cellsRadius;
    private Snake snake;
    private Pizza pizza;

    private String highScoreKey = "highScore";
    private String bestUserKey = "bestUser";
    private long highScore;
    private int score = 0;
    private String bestUser;
    private boolean gameSaved;

    private String username = "Yani";

    private DBHelper dataBase;

    private Bitmap mur, snakeCell, snakeCellHead, pizzaBitmap;

    protected final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        this.setContentView(R.layout.game);
        this.surface = findViewById(R.id.game_surface);

        final SwipeDetector swipeDetector = new SwipeDetector(this);
        this.surface.setOnTouchListener(swipeDetector);
        this.surface.setOnLongClickListener(swipeDetector);

        this.loadBitmaps();
        surface.setFocusable(true);

        this.username = intent.getStringExtra("username");
        this.findViewById(R.id.return_home).setVisibility(View.GONE);
        this.dataBase = new DBHelper(this);
        this.initPartie();
    }


    private final void loadBitmaps() {
        Bitmap src = BitmapFactory.decodeResource(getResources(), R.mipmap.mur);
        mur = this.addShadow(src, src.getHeight(), src.getWidth(), Color.BLACK, 10, 6, 6);

        src = BitmapFactory.decodeResource(getResources(), R.mipmap.snake_simple);
        snakeCell = this.addShadow(src, src.getHeight(), src.getWidth(), Color.BLACK, 10, 6, 6);

        src = BitmapFactory.decodeResource(getResources(), R.mipmap.snake_head);
        snakeCellHead = this.addShadow(src, src.getHeight(), src.getWidth(), Color.BLACK, 10, 6, 6);

        src = BitmapFactory.decodeResource(getResources(), R.mipmap.pizza);
        pizzaBitmap = this.addShadow(src, src.getHeight(), src.getWidth(), Color.BLACK, 10, 6, 6);
    }

    public final Bitmap addShadow(final Bitmap bm, final int dstHeight, final int dstWidth, int color, int size, float dx, float dy) {
        final Bitmap mask = Bitmap.createBitmap(dstWidth, dstHeight, Bitmap.Config.ALPHA_8);

        final Matrix scaleToFit = new Matrix();
        final RectF src = new RectF(0, 0, bm.getWidth(), bm.getHeight());
        final RectF dst = new RectF(0, 0, dstWidth - dx, dstHeight - dy);
        scaleToFit.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER);

        final Matrix dropShadow = new Matrix(scaleToFit);
        dropShadow.postTranslate(dx, dy);

        final Canvas maskCanvas = new Canvas(mask);
        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        maskCanvas.drawBitmap(bm, scaleToFit, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
        maskCanvas.drawBitmap(bm, dropShadow, paint);

        final BlurMaskFilter filter = new BlurMaskFilter(size, BlurMaskFilter.Blur.NORMAL);
        paint.reset();
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setMaskFilter(filter);
        paint.setFilterBitmap(true);

        final Bitmap ret = Bitmap.createBitmap(dstWidth, dstHeight, Bitmap.Config.ARGB_8888);
        final Canvas retCanvas = new Canvas(ret);
        retCanvas.drawBitmap(mask, 0,  0, paint);
        retCanvas.drawBitmap(bm, scaleToFit, null);
        mask.recycle();
        return ret;
    }

    public final void initPartie() {

        Partie.this.findViewById(R.id.return_home).setVisibility(View.GONE);
        this.paint = new Paint();
        this.directionsQueue = new ArrayDeque<Direction>();
        this.gameSaved = false;
        this.score = 0;

        this.directionsQueue.clear();
        float fieldWidth = 17f;

        final Display display = getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        this.cellsDiameter = round((size.x / fieldWidth));
        this.cellsRadius = cellsDiameter / 2;
        float fieldHeight = round(size.y / cellsDiameter);
        this.dimensions = new PointF(fieldWidth, fieldHeight);
        this.snake = new Snake(cellsRadius, this, (int)fieldWidth/2, (int)fieldHeight/2);

        this.generateNewPizza();

        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        this.highScore = sharedPref.getLong(highScoreKey, 0);
        this.bestUser = sharedPref.getString(bestUserKey, "Yani");

        TimerManager.getNewTimeManager(this.surface.getHolder(), this);

    }

    public final void update() {

        if (this.snake.isAlive()) {
            this.snake.move();
            this.toucheWall();
            this.eatPizza();
        }

    }

    public final void bottom2top() {
        if (this.snake.getDirection().isHorizontal()) {
            this.snake.setDirection(Direction.UP);

        }
    }

    public final void top2bottom() {
        if (this.snake.getDirection().isHorizontal()) {
            this.snake.setDirection(Direction.DOWN);

        }

    }

    public final void left2right() {
        if (this.snake.getDirection().isVertical()) {
            this.snake.setDirection(Direction.RIGHT);

        }
    }

    public final void right2left() {
        if (this.snake.getDirection().isVertical()) {
            this.snake.setDirection(Direction.LEFT);

        }

    }

    private final void generateNewPizza() {
        this.pizza = new Pizza(this.dimensions, this.snake, this.cellsRadius);
    }


    private final void toucheWall() {
        final PointF head = this.snake.getHead().getLocation();

        switch (this.snake.getDirection()) {
            case UP:
                if (head.y < 1){
                    this.snake.kill();

                }
                break;
            case DOWN:
                if (head.y > dimensions.y - 2){
                    this.snake.kill();
                }
                break;
            case LEFT:
                if (head.x < 1){
                    this.snake.kill();
                }
                break;
            case RIGHT:
                if (head.x > dimensions.x - 2) {
                    this.snake.kill();
                }
                break;
        }
    }

    public final void eatPizza() {
        if (this.snake.ate(this.pizza)) {
            SoundManager.play(this, R.raw.manger);
            this.snake.incSize();
            TimerManager.getTimer().incSpeed();
            this.score++;
            if (this.score > this.highScore) {
                this.highScore = this.score;
                this.bestUser = this.username;
                this.saveScore();
            } this.generateNewPizza();
        }
    }

    private final void saveScore() {
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(this.highScoreKey, this.highScore);
        editor.putString(this.bestUserKey, this.bestUser);
        editor.apply();
    }

    public final void render(final Canvas canvas) {

        this.drawBackground(canvas);

        this.drawBoardLimits(canvas);

        if (!this.snakeIsDead()) {
            this.drawScore(canvas);
            this.drawPizza(canvas);
            this.drawSnake(canvas);

        } else {
            this.drawPizza(canvas);
            this.drawSnake(canvas);
            this.drawScore(canvas);

        }

    }

    private final void drawBackground(final Canvas canvas) {
        int bgColor;
        if (this.stateAsk.equals(TimerManager.StateAsk.WAIT)) {
            bgColor = Color.parseColor("#7a7878");
        } else {
            bgColor = this.snake.isAlive() ? Color.parseColor("#7a5049") : Color.parseColor("#221e1e");
        }

        this.paint.setColor(bgColor);
        canvas.drawRect(0, 0, this.dimensions.x * this.cellsDiameter, this.dimensions.y * this.cellsDiameter, this.paint);
    }

    private final void drawCell(final Canvas canvas, final PointF p, final Bitmap bitmap) {
        final Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final float x = p.x * cellsDiameter;
        final float y = p.y * cellsDiameter;
        final RectF dst = new RectF(x, y, x + this.cellsDiameter, y + this.cellsDiameter);
        canvas.drawBitmap(bitmap, src, dst, this.paint);
    }

    private final void drawBoardLimits(final Canvas canvas) {
        for (float i = 0; i < dimensions.x; i++) {
            this.drawCell(canvas, new PointF(i, 0), this.mur);
            this.drawCell(canvas, new PointF(i, this.dimensions.y - 1), this.mur);
        } for (int i = 0; i < round(dimensions.y); i++) {
            this.drawCell(canvas, new PointF(0, i), this.mur);
            this.drawCell(canvas, new PointF(this.dimensions.x - 1, i), this.mur);
        }
    }

    private float round(final float nb) {
        return (float)((int)nb);
    }

    private final void drawPizza(final Canvas canvas) {
        this.drawCell(canvas, this.pizza.getLocation(), this.pizzaBitmap);
    }

    private final void drawSnake(final Canvas canvas) {
        for (int i = 1; i< this.snake.getaCases().size(); i++) {
            this.drawCell(canvas, this.snake.getaCases().get(i).getLocation(), this.snakeCell);
        } this.drawCell(canvas, this.snake.getaCases().get(0).getLocation(), this.snakeCellHead);
    }

    private final void drawScore(final Canvas canvas) {
        final String[] text = new String[]{"Best " + this.highScore + " - By " + this.bestUser,
                "Score: " + this.score};

        final int textSize = (int)(3 * this.cellsDiameter / 2);
        this.paint.setColor(Color.parseColor("#bebebe"));
        this.paint.setTextSize(textSize);
        this.paint.setTypeface(Typeface.create("Arial", Typeface.BOLD));

        if (this.snake.isAlive()) {
            this.drawCenter(canvas, this.paint, text);
        } else {
            this.drawEnd(canvas);
        }
    }

    private final void drawEnd(final Canvas canvas) {
        final String[] text = new String[]{"Best " + this.highScore + " - By " + this.bestUser,
                "Score: " + this.score};
        this.drawCenter(canvas, this.paint, new String[]{text[0], text[1], "Game Over", "Click to restart"});
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Button home = Partie.this.findViewById(R.id.return_home);
                home.setVisibility(View.VISIBLE);

                home.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (Partie.this.gameSaved) {
                            Intent intent = new Intent(Partie.this, BestUsers.class);
                            intent.putExtra("username", Partie.this.username);
                            Partie.this.startActivity(intent);
                        }
                    }
                });
            }
        });

        if (!this.gameSaved) {
            this.dataBase.insertScore(this.username, this.score);
            this.gameSaved = true;
        }
    }


    private final void drawCenter(final Canvas canvas, final Paint paint, final String[] textes) {
        final float milieuY = this.surface.getHeight() / 2;
        int i = 0;
        final int margin = this.surface.getHeight()/40;
        final int totalMargin = margin * textes.length;
        while (i<textes.length) {
            final String text = textes[i];
            final Rect r = new Rect(0, 0, this.surface.getWidth(), this.surface.getHeight());
            canvas.getClipBounds(r);
            final int cWidth = r.width();
            paint.setTextAlign(Paint.Align.LEFT);
            paint.getTextBounds(text, 0, text.length(), r);
            final float x = cWidth / 2f - r.width() / 2f - r.left;
            final float y = milieuY - (r.height()*(textes.length/2)) + r.height()*i - totalMargin/2 + margin *i;
            canvas.drawText(text, x, y, paint);
            i++;
        }

    }

    public final boolean snakeIsDead() {
        return !this.snake.isAlive();
    }

    public final boolean isPaused() {
        return this.stateAsk.equals(TimerManager.StateAsk.WAIT);
    }

    public final void changeState() {
        if (this.stateAsk.equals(TimerManager.StateAsk.WAIT)) {
            this.stateAsk = TimerManager.StateAsk.PLAY;
            TimerManager.getTimer().wakeUp();
            SoundManager.play(this, R.raw.pause);
        } else {
            this.stateAsk = TimerManager.StateAsk.WAIT;
            SoundManager.play(this, R.raw.pause);
        }
    }

    public final TimerManager.StateAsk getState() {
        return this.stateAsk;
    }

    public final long deadSince() {
        return this.snake.deadSince();
    }

    @Override
    public void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}