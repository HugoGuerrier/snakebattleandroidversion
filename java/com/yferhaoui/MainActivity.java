package com.yferhaoui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.security.MessageDigest;


public class MainActivity extends Activity {

    private EditText pseudo;
    private TextView password;

    private DBHelper dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.activity_main);
        this.dataBase = new DBHelper(this);

        ((TextView)findViewById(R.id.header_text)).setText(this.loadBestUser());

        this.pseudo = findViewById(R.id.pseudo);
        this.password = findViewById(R.id.password);

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                final String username = MainActivity.this.pseudo.getText().toString();
                final String pass = MainActivity.this.password.getText().toString();
                if (!username.equals("") && !pass.equals("")) {

                    if (username.length() < 7) {
                        if (MainActivity.this.dataBase.contains("username", username)) {
                            if (MainActivity.sha256(pass).equals(MainActivity.this.dataBase.getPlayerPass(username))) {
                                MainActivity.this.hideKeyboard(MainActivity.this);
                                Intent intent = new Intent(MainActivity.this, BestUsers.class);
                                intent.putExtra("username", MainActivity.this.pseudo.getText().toString());
                                MainActivity.this.startActivity(intent);
                            } else {
                                Toast.makeText(MainActivity.this, "Wrong password for the user : " + username, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            MainActivity.this.hideKeyboard(MainActivity.this);
                            Intent intent = new Intent(MainActivity.this, BestUsers.class);
                            intent.putExtra("username", MainActivity.this.pseudo.getText().toString());
                            MainActivity.this.dataBase.insertPlayer(username, MainActivity.sha256(pass));
                            startActivity(intent);
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Your username is too long (max 6 characters)" , Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(MainActivity.this, "Please enter a " + (username.equals("") ? "Username" : "Password"), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public static void hideKeyboard(final Activity activity) {
        final InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        } imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private String loadBestUser() {
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        return String.valueOf("Best : " + sharedPref.getLong("highScore", 0) + " - From : " + sharedPref.getString("bestUser", "Yani"));
    }

    public final static String sha256(final String base) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
