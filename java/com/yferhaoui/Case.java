package com.yferhaoui;
import android.graphics.PointF;

import java.util.ArrayList;
import java.util.List;

public class Case extends ObjectPartie {

    List<Case> lastLocations = new ArrayList<Case>();

    public Case(Float x, Float y, Float radius) {
        super(radius);
        location = new PointF(x, y);
    }

    public Case(final Case aCase) {
        super(aCase.getRadius());
        location = new PointF(aCase.getLocation().x, aCase.getLocation().y);
    }

    public void addPos(final Case aCase) {
        this.lastLocations.add(0, aCase);
    }

    public Case getLastLocation() {
        this.lastLocations.remove(5);
        return new Case(this.lastLocations.get(4));
    }
}
