package com.yferhaoui;

import android.content.Context;
import android.media.MediaPlayer;

public class SoundManager {

    private static MediaPlayer mp;

    private static MediaPlayer mp1;

    private static MediaPlayer mp2;

    private static MediaPlayer mp3;

    private static int nextMedi = 0;

    public static void play(final Context context, final int id) {

        try {
            if (SoundManager.nextMedi == 0) {
                if (SoundManager.mp != null) {
                    mp.release();
                }

                SoundManager.mp = MediaPlayer.create(context, id);
                SoundManager.mp.start();
                SoundManager.nextMedi++;
            } else if (SoundManager.nextMedi == 1) {
                if (SoundManager.mp1 != null) {
                    mp1.release();
                }

                SoundManager.mp1 = MediaPlayer.create(context, id);
                SoundManager.mp1.start();
                SoundManager.nextMedi++;
            } else if (SoundManager.nextMedi == 2) {
                if (SoundManager.mp2 != null) {
                    mp2.release();
                }

                SoundManager.mp2 = MediaPlayer.create(context, id);
                SoundManager.mp2.start();
                SoundManager.nextMedi++;
            } else {
                if (SoundManager.mp3 != null) {
                    mp3.release();
                }

                SoundManager.mp3 = MediaPlayer.create(context, id);
                SoundManager.mp3.start();
                SoundManager.nextMedi = 0;
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }
}