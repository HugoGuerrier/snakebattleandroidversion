package com.yferhaoui;

public enum Direction {
    RIGHT(0), DOWN(1), LEFT(2), UP(3);

    private final int direction;

    Direction(int direction) {
        this.direction = direction;
    }

    public int getValue() {
        return direction;
    }

    public boolean isHorizontal() {
        return direction == 0 || direction == 2;
    }

    public boolean isVertical() {
        return direction == 1 || direction == 3;
    }

}
