package com.yferhaoui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class BestUsers extends Activity {

    private List<Player> classement = new ArrayList<Player>();
    private DBHelper dataBase;

    private String username;

    private Button play = null;
    private Button exit = null;
    private Button account = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.best_users);

        final Intent intent = getIntent();
        this.username = intent.getStringExtra("username");
        ((TextView)this.findViewById(R.id.username)).setText(this.username);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        this.play = this.findViewById(R.id.add);
        this.play.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                BestUsers.this.play();
            }
        });

        this.account = this.findViewById(R.id.account);
        this.account.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                Intent intent = new Intent(BestUsers.this, Account.class);
                intent.putExtra("username", BestUsers.this.username);
                startActivity(intent);
            }
        });

        this.exit = this.findViewById(R.id.send_to);
        this.exit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                BestUsers.this.exit();
            }
        });

        this.dataBase = new DBHelper(this);
        this.loadClassement();

        this.displayClassement();
    }

    private final void loadClassement() {
        this.classement = this.dataBase.getClassement();
    }

    private final void displayClassement() {
        final LinearLayout layout = findViewById(R.id.classement);
        layout.removeAllViews();

        for (final Player player : this.classement) {
            final View child = getLayoutInflater().inflate(R.layout.player, null);
            layout.addView(child);

            ((TextView)child.findViewById(R.id.username)).setText(player.username);

            ((TextView)child.findViewById(R.id.thedate)).setText(player.getDate());

            ((TextView)child.findViewById(R.id.score)).setText(String.valueOf(player.getScore()));

            if (player.username.equals(this.username)) {
                child.setBackgroundColor(this.getResources().getColor(R.color.selected));
            }

        }

    }

    private final void play() {
        Intent intent = new Intent(this, Partie.class);
        intent.putExtra("username", this.username);
        startActivity(intent);
    }

    private final void exit() {
        Intent intent = new Intent(BestUsers.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume()  {
        super.onResume();
        this.displayClassement();
    }

    @Override
    public void onRestart()  {
        super.onRestart();
        this.displayClassement();
    }

}
