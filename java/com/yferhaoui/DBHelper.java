package com.yferhaoui;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "classement.db";
    public static final String TABLE_NAME = "player";
    public final static long msPerDay = 86400000;

    public DBHelper(final Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public final void onCreate(final SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS player " +
                "(username  TEXT        PRIMARY KEY, " +
                "password   TEXT        NOT NULL) "
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS score " +
                "(username  TEXT        NOT NULL, " +
                "score      INTEGER     DEFAULT 0, " +
                "thedate    TIMESTAMP   NOT NULL," +
                "FOREIGN KEY(username)  REFERENCES player(username))"
        );
    }

    @Override
    public final void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS player");
        db.execSQL("DROP TABLE IF EXISTS score");
        onCreate(db);
    }

    public final boolean insertPlayer(final String username, final String password) {
        final SQLiteDatabase db = this.getWritableDatabase();
        final ContentValues contentValues = new ContentValues();
        contentValues.put("username", username);
        contentValues.put("password", password);
        db.insertWithOnConflict("player", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        return true;
    }

    public final boolean insertScore(final String username, final int score) {
        final SQLiteDatabase db = this.getWritableDatabase();
        final ContentValues contentValues = new ContentValues();
        contentValues.put("username", username);
        contentValues.put("score", score);
        contentValues.put("thedate", System.currentTimeMillis());
        db.insertWithOnConflict("score", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        this.deleteOldScore();
        return true;
    }

    private final boolean deleteOldScore() {
        final SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from score where thedate < " + (System.currentTimeMillis()-DBHelper.msPerDay*5) + " and thedate not in " +
                "(SELECT thedate FROM score GROUP BY username order by score desc, thedate LIMIT 1)");
        return true;
    }

    public final Cursor getData(final String username) {
        final SQLiteDatabase db = this.getReadableDatabase();
        final Cursor res =  db.rawQuery( "select * from player where username="+username+"", null );
        return res;
    }

    public final int numberOfRows() {
        final SQLiteDatabase db = this.getReadableDatabase();
        return (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
    }

    public final boolean updatePassword (final String username, final String password) {
        final SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("password", password);
        db.update("player", contentValues, "username = ? ", new String[] { username } );
        return true;
    }

    public final String getPlayerPass(final String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.stringForQuery(db, "SELECT password FROM player where username = '" + username + "'", null);
    }

    public final int getPlayerBestScore(final String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        return (int)DatabaseUtils.longForQuery(db, "SELECT MAX(score) FROM score where username = '" + username + "'", null);
    }

    public final boolean contains(final String key, final String value) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM player where " + key + " = '" + value + "'", null) > 0;
    }

    public final Integer deletePlayer (final String username) {
        final SQLiteDatabase db = this.getWritableDatabase();
        db.delete("score","username = ? ", new String[] { username });
        return db.delete("player","username = ? ", new String[] { username });
    }

    public final List<Player> getClassement() {
        final List<Player> players = new ArrayList<Player>();

        SQLiteDatabase db = this.getReadableDatabase();
        final Cursor res =  db.rawQuery( "SELECT username, thedate, MAX(score) as bestscore FROM score GROUP BY username order by bestscore desc, thedate", null );
        res.moveToFirst();

        while(res.isAfterLast() == false) {
            players.add(new Player(res));
            res.moveToNext();
        }
        return players;
    }

    public final List<Player> getUserClassement(String username) {
        final List<Player> players = new ArrayList<Player>();

        SQLiteDatabase db = this.getReadableDatabase();
        final Cursor res =  db.rawQuery( "SELECT username, thedate, score as bestscore FROM score where username = '" + username + "' order by thedate desc", null );
        res.moveToFirst();

        while(res.isAfterLast() == false) {
            players.add(new Player(res));
            res.moveToNext();
        }
        return players;
    }

}
