package com.yferhaoui;

import java.util.concurrent.ThreadLocalRandom;

public class RandomNumber {

    public static long nextLong(final long nb) {
        if (nb > 0) {
            return ThreadLocalRandom.current().nextLong(nb);
        } return 0;
    }

    public static long nextLong(final long min, final long max) {
        if ( (max-min) > 0) {
            return ThreadLocalRandom.current().nextLong(min, max);
        } return 0;
    }

    public static int nextInteger(final int nb) {
        if (nb > 0) {
            return ThreadLocalRandom.current().nextInt(nb);
        } return 0;
    }

    public static int nextInteger(final int min, final int max) {
        if ( (max-min) > 0) {
            return ThreadLocalRandom.current().nextInt(min, max);
        } return 0;
    }

    public static double nextDouble(final double nb) {
        if (nb > 0.0) {
            return ThreadLocalRandom.current().nextDouble(nb);
        } return 0.0;
    }

    public static double nextDouble(final double min, final double max) {
        if ( (max-min) > 0.0) {
            return ThreadLocalRandom.current().nextDouble(min, max);
        } return 0.0;
    }

    public static float nextFloat(final float nb) {
        return (float)RandomNumber.nextDouble((double)nb);
    }

    public static float nextFloat(final float min, final float max) {
        return (float)RandomNumber.nextDouble(min, max);
    }
}
