package com.yferhaoui;

import android.content.Context;
import android.graphics.PointF;


import java.util.ArrayList;
import java.util.List;

public class Snake {

    private ArrayList<Case> aCases;
    private Float radius;

    private Direction direction;
    private List<Direction> directionAsk;
    private boolean alive = true;
    private int score;
    private long deadSince;

    private final Context context;

    protected Snake(final Float radius, final Context context, final int xStart, final int yStart) {
        this.context = context;
        this.radius = radius;

        aCases = new ArrayList<Case>();
        direction = Direction.DOWN;
        this.directionAsk = new ArrayList<Direction>();

        aCases.add(new Case(round((float)xStart), round((float)yStart), radius));
        float i = 1.1f;
        while (i > 0) {
            aCases.get(0).addPos(new Case(round((float)xStart), round(yStart - i), radius));
            i = i-0.2f;
        }

        PointF head = aCases.get(0).getLocation();
        aCases.add(new Case(aCases.get(0).getLastLocation()));
        i = 1.1f;
        while (i > 0) {
            aCases.get(1).addPos(new Case(round((float)xStart), round(head.y - i), radius));
            i = i-0.2f;
        }

        PointF headB = aCases.get(1).getLocation();
        aCases.add(new Case(aCases.get(1).getLastLocation()));
        i = 1.1f;
        while (i > 0) {
            aCases.get(1).addPos(new Case(round((float)xStart), round(headB.y - i), radius));
            i = i-0.2f;
        }

        score = 0;
    }

    public final void move() {

        Case headB = getHead();

        getHead().addPos(new Case(headB));
        int x = (int)(round(getHead().getLocation().x) * 10.0);
        int y = (int)(round(getHead().getLocation().y) * 10.0);
        if ( x % 10 == 0 && y % 10 == 0) {
            if (this.directionAsk.size() != 0) {
                this.direction = this.directionAsk.get(0);
                this.directionAsk.remove(0);
            }
        }

        switch (direction) {
            case UP:
                headB.getLocation().y = round(headB.getLocation().y) - 0.2f;
                break;
            case DOWN:
                headB.getLocation().y = round(headB.getLocation().y) + 0.2f;
                break;
            case LEFT:
                headB.getLocation().x = round(headB.getLocation().x) - 0.2f;
                break;
            case RIGHT:
                headB.getLocation().x = round(headB.getLocation().x) + 0.2f;
                break;
        }


        int i = 1;
        while (i<this.aCases.size()) {
            Case theCase = this.aCases.get(i-1).getLastLocation();
            this.aCases.get(i).addPos(new Case(this.aCases.get(i)));
            this.aCases.get(i).getLocation().x = theCase.getLocation().x;
            this.aCases.get(i).getLocation().y = theCase.getLocation().y;
            i++;
        }

        this.eatSnake();
    }

    private float round(final Float nb) {
        return Math.round(nb*10f)/10f;
    }

    private final void eatSnake() {
        for (final Case aCase : aCases) {
            if (aCase != getHead() && aCase.getLocation().equals(getHead().getLocation())) {
                this.kill();
            }
        }
    }

    public final boolean ate(final ObjectPartie element) {
        return this.getHead().inCase(element);
    }

    public final void incSize() {
        aCases.add(new Case(aCases.get(aCases.size()-1).getLastLocation()));
        float i = 1.1f;
        while (i > 0) {
            aCases.get(aCases.size()-1).addPos(new Case(round(aCases.get(aCases.size()-1).getLocation().x),
                    round(aCases.get(aCases.size()-1).getLocation().x - 1 - i), radius));
            i = i-0.1f;
        }
    }

    public final ArrayList<Case> getaCases() {
        return aCases;
    }

    public final Case getHead() {
        return aCases.get(0);
    }

    public final Direction getDirection() {
        return direction;
    }

    public final void setDirection(final Direction direction) {
        this.directionAsk.add(direction);
        switch(this.directionAsk.get(directionAsk.size()-1).getValue()) {
            case 0 :
                SoundManager.play(this.context, R.raw.haut);
                break;
            case 1 :
                SoundManager.play(this.context, R.raw.bas);
                break;
            case 2 :
                SoundManager.play(this.context, R.raw.gauche);
                break;
            case 3 :
                SoundManager.play(this.context, R.raw.droite);
                break;

        } if (this.directionAsk.size() > 2) {
            this.directionAsk.remove(0);
        }
    }

    public final void kill() {
        this.alive = false;
        this.deadSince = System.currentTimeMillis();
        SoundManager.play(this.context, R.raw.mort);
    }

    public final boolean isAlive() {
        return this.alive;
    }

    public final long deadSince() {
        return this.deadSince;
    }

}
