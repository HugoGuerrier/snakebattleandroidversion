package com.yferhaoui;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;

public class TimerManager extends Thread {

    public final static void getNewTimeManager(final SurfaceHolder surfaceHolder, final Partie game) {
        TimerManager.timeManager = new TimerManager(surfaceHolder, game);
        TimerManager.timeManager.start();
    }

    public final static TimerManager getTimer() {
        return TimerManager.timeManager;
    }

    private static TimerManager timeManager = null;

    enum StateAsk {
        WAIT,
        PLAY
    }

    private Canvas canvas;

    private long sleepTime = 50;
    private final long MIN_SLEEP = 15;

    private final SurfaceHolder surfaceHolder;

    private final Partie game;

    private TimerManager(final SurfaceHolder surfaceHolder, final Partie game) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.game = game;
    }

    @Override
    public void run() {

        while (!this.game.snakeIsDead()) {

            while (this.game.getState().equals(StateAsk.PLAY) && !this.game.snakeIsDead()) {

                this.canvas = this.surfaceHolder.lockCanvas();

                final long beginTime = System.currentTimeMillis();

                this.game.update();

                if (this.canvas!= null) {
                    this.game.render(canvas);
                }

                final long timeDiff = System.currentTimeMillis() - beginTime;
                final long sleepTime = (int) (this.sleepTime - timeDiff);
                Log.d("", String.valueOf(sleepTime));
                try {
                    Thread.sleep(Math.max(0, sleepTime));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (this.canvas != null) {
                    this.surfaceHolder.unlockCanvasAndPost(this.canvas);
                }
            }

            while (this.game.getState().equals(StateAsk.WAIT) && !this.game.snakeIsDead()) {

                this.canvas = this.surfaceHolder.lockCanvas();

                if (this.canvas!= null) {
                    this.game.render(this.canvas);
                    this.surfaceHolder.unlockCanvasAndPost(this.canvas);
                }

                this.waitPlay();
            }
        }
        this.canvas = this.surfaceHolder.lockCanvas();
        this.game.render(this.canvas);
        this.surfaceHolder.unlockCanvasAndPost(this.canvas);
    }

    public final void incSpeed() {
        if (this.sleepTime > 15) {
            this.sleepTime--;
        }
    }

    public final synchronized void wakeUp() {
        this.notifyAll();
    }

    private final synchronized void waitPlay() {
        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
