package com.yferhaoui;

import android.view.MotionEvent;
import android.view.View;

public class SwipeDetector implements View.OnTouchListener, View.OnLongClickListener {

    private int min_distance = 30;
    private float downX, downY, upX, upY;
    private Partie partie;

    public SwipeDetector(Partie partie){
        this.partie=partie;
    }

    @Override
    public boolean onLongClick(View arg0) {

        this.partie.changeState();
        return false;

    }


    public boolean onTouch(final View v, final MotionEvent event) {

        if (this.partie.snakeIsDead() == false && this.partie.isPaused() == false) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    downX = event.getX();
                    downY = event.getY();
                    return false;

                case MotionEvent.ACTION_UP:
                    upX = event.getX();
                    upY = event.getY();

                    float deltaX = downX - upX;
                    float deltaY = downY - upY;

                    if(Math.abs(deltaX) > Math.abs(deltaY)) { //HORIZONTAL SCROLL
                        if(Math.abs(deltaX) > min_distance) {
                            // left or right
                            if(deltaX < 0) {

                                this.partie.left2right();
                                return false;
                            } else if(deltaX > 0) {

                                this.partie.right2left();
                                return false;
                            }
                        } return false;
                    } else { //VERTICAL SCROLL

                        if(Math.abs(deltaY) > min_distance){
                            // top or down
                            if(deltaY < 0) {
                                this.partie.top2bottom();
                                return false;
                            } else if(deltaY > 0) {
                                this.partie.bottom2top();
                                return false;
                            }
                        } return false;
                    }
            }
        } else if (this.partie.snakeIsDead() && System.currentTimeMillis() - this.partie.deadSince() > 1000 ){
                this.partie.initPartie();
        } return false;
    }


}
