package com.yferhaoui;

import android.database.Cursor;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Player {

    public final String username;

    private int score;
    private long thedate;

    public Player(final Cursor res) {
        this.username = res.getString(res.getColumnIndex("username"));
        this.score = res.getInt(res.getColumnIndex("bestscore"));
        this.thedate = res.getLong(res.getColumnIndex("thedate"));
    }

    public final int getScore() {
        return this.score;
    }

    public final String getDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date(this.thedate));
    }
}
