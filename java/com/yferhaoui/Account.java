package com.yferhaoui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Account extends Activity {

    private List<Player> classement = new ArrayList<Player>();
    private DBHelper dataBase;

    private String username;

    private Button play = null;
    private Button exit = null;
    private Button home = null;

    private EditText pass1 = null;
    private EditText pass2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account);

        final Intent intent = getIntent();
        this.username = intent.getStringExtra("username");
        ((TextView)this.findViewById(R.id.username)).setText(this.username);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        this.play = this.findViewById(R.id.add);
        this.play.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                Account.this.play();
            }
        });

        this.home = this.findViewById(R.id.home);
        this.home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                Intent intent = new Intent(Account.this, BestUsers.class);
                intent.putExtra("username", Account.this.username);
                startActivity(intent);
            }
        });

        this.exit = this.findViewById(R.id.send_to);
        this.exit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                Account.this.exit();
            }
        });

        this.pass1 = this.findViewById(R.id.currentpass);
        this.pass2 = this.findViewById(R.id.newpassword);

        this.dataBase = new DBHelper(this);

        this.findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                final String currentPass = Account.this.pass1.getText().toString();
                final String newPass = Account.this.pass2.getText().toString();
                if (!newPass.equals("") && !newPass.equals("")) {

                    if (MainActivity.sha256(currentPass).equals(Account.this.dataBase.getPlayerPass(Account.this.username))) {
                        Account.this.dataBase.updatePassword(Account.this.username, MainActivity.sha256(newPass));
                        Toast.makeText(Account.this, "Password changed ! ", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(Account.this, "Wrong current password for the user : " + username, Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(Account.this, "Please enter a new valid Password", Toast.LENGTH_LONG).show();
                }
            }
        });

        this.findViewById(R.id.myscore).setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                final Intent intent = new Intent(Account.this, MyScores.class);
                intent.putExtra("username", Account.this.username);
                startActivity(intent);
            }
        });
        //this.loadClassement();

        //this.displayClassement();
    }
/*
    private final void loadClassement() {
        this.classement = this.dataBase.getClassement();
    }

    private final void displayClassement() {
        final LinearLayout layout = findViewById(R.id.classement);
        layout.removeAllViews();

        for (final Player player : this.classement) {
            final View child = getLayoutInflater().inflate(R.layout.player, null);
            layout.addView(child);

            ((TextView)child.findViewById(R.id.username)).setText(player.username);

            ((TextView)child.findViewById(R.id.thedate)).setText(player.getDate());

            ((TextView)child.findViewById(R.id.score)).setText(String.valueOf(player.getScore()));

            if (player.username.equals(this.username)) {
                child.setBackgroundColor(this.getResources().getColor(R.color.selected));
            }

        }

    }*/

    private final void play() {
        Intent intent = new Intent(this, Partie.class);
        intent.putExtra("username", this.username);
        startActivity(intent);
    }

    private final void exit() {
        Intent intent = new Intent(Account.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume()  {
        super.onResume();
        //this.displayClassement();
    }

    @Override
    public void onRestart()  {
        super.onRestart();
        //this.displayClassement();
    }

}